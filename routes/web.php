<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SolicitantesController;
use App\Http\Controllers\IngresosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('inicio', [SolicitantesController::class, "index"])->name("crud.index");
Route::post('agregar_solicitante', [SolicitantesController::class, "agregar_solicitante"])->name("crud.create.solicitante");
Route::post('update_solicitante', [SolicitantesController::class, "update_solicitante"])->name("crud.update.solicitante");
Route::get('editar/solicitante/{id}', [SolicitantesController::class, "editar_solicitante"])->name("crud.editar.solicitante");
Route::post('agregar_ingreso', [IngresosController::class, "agregar_ingreso"])->name("crud.create.ingreso");
// Route::get('view_editar_solicitante', [SolicitantesController::class, "view_editar_solicitante"])->name("crud.view.editar.solicitante'");

<!-- @extends('layout') -->
<!-- @section('titulo','Crear nuevo registro') -->
@section('content')

<body>

          <!-- <form method="POST" action="{{ route('crud.update.solicitante') }}" class="row g-3"> -->
          <form  class="row g-3">
              <div class="col-4">              
                  <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{ $data_solicitante[0]->nombre }}" required>            
                    <p id="p_nombre" class="text text-danger" color="red"></p>            
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="apellidoP" placeholder="Apellido paterno" value="{{ $data_solicitante[0]->apellido_paterno }}" required>
                  <p id="p_apellidoP" class="text text-danger" color="red"></p>               
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="apellidoM" placeholder="Apellido materno" value="{{ $data_solicitante[0]->apellido_materno }}" required>
                  <p id="p_apellidoM" class="text text-danger" color="red"></p>
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="edad" placeholder="Edad" value="{{ $data_solicitante[0]->edad }}">
                  <p id="p_edad" class="text text-danger" color="red"></p>
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="sexo" placeholder="Sexo" value="{{ $data_solicitante[0]->sexo }}">
                  <p id="p_sexo" class="text text-danger" color="red"></p>
              </div>
              <div class="col-4">
                  <input type="date" class="form-control" id="fechaNac" placeholder="Fecha nacimiento" value="{{ $data_solicitante[0]->fecha_nacimiento }}">
                  <p id="p_fechaNac" class="text text-danger" color="red"></p>
              </div>
              <div class="col-md-6">
                  <input type="email" class="form-control" id="inputEmail" placeholder="Correo electronico" value="{{ $data_solicitante[0]->email }}">
                  <p id="p_email" class="text text-danger" color="red"></p>
              </div>
              <div class="col-md-6">
                  <input type="text" class="form-control" id="inputCurp" placeholder="Curp" value="{{ $data_solicitante[0]->curp }}">
                  <p id="p_curp" class="text text-danger" color="red"></p>
              </div>
              <div class="col-12">
                  <input type="text" class="form-control"id="inputDomicilio"  placeholder="Domicilio completo" value="{{ $data_solicitante[0]->domicilio }}">
                  <p id="p_domicilio" class="text text-danger" color="red"></p>
              </div>  
              <div class="btn-group" role="group" aria-label="Basic example"> 
              <button type="button" class="btn btn-primary"  onclick='update({{ $data_solicitante[0]->id }});'>Guardar datos</button>
              <a href="{{ route('crud.index') }}" class="redondo btn btn-danger"><i class="fas fa-ban"></i> Cancelar</a>
              <!-- <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button> -->
              </div>
          </form>
          <hr>
          <h3 class="text-center p-3">Tabla de ingresos</h3><br>
          <button class="btn btn-secondary" data-bs-target="#ModalParaIngresos" data-bs-toggle="modal" data-bs-dismiss="modal">Agregar otro ingreso</button><br><br>
          <!-- <button type="button" class="btn btn-secondary"  onclick='addIngreso();'>Agregar ingreso</button><br><br> -->
            <table id="tbl-ingresos" class="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">Nombre empresa</th>
                    <th scope="col">Tipo comprobante</th>
                    <th scope="col">Salario bruto</th>
                    <th scope="col">Salario neto</th>
                    <th scope="col">Tipo empleo</th>
                    <th scope="col">Fecha ingreso</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data_solicitante[0]->ingresos as $item)
                    <tr>
                    <td>{{$item->nombre_empresa}}</td>
                    <td>{{$item->tipo_comprobante}}</td>
                    <td>{{$item->salario_bruto}}</td>
                    <td>{{$item->salario_neto}}</td>
                    <td>{{$item->tipo_empleo}}</td>
                    <td>{{$item->fecha_ingreso}}</td>
                    <!-- <td>
                    <a href="editar/solicitante/{{$item->id}}" class="btn btn-warning " ><i  class="bi-pen"></i></a>
                    </td> -->
   
                    </tr>
                @endforeach
    
                </tbody>
            </table>
  
            @section('modalIngresos')
              @include('modalAddIngresos')
            @show

<!-- <a class="btn btn-primary" data-bs-toggle="modal" href="#exampleModal" role="button">Open first modal</a> -->

<script>

document.getElementsByClassName("text-danger"); 
nombre.classList.remove("text-danger"); 

let array = [];
let myModal = document.getElementById('exampleModalToggle');
let tbody = document.querySelector('#tbl-ingresos tbody');
let div_alert = document.getElementById("div_alert");
let p_nombre = document.getElementById("p_nombre");
let p_apellidoP = document.getElementById("p_apellidoP");
let p_apellidoM = document.getElementById("p_apellidoM");
let p_edad = document.getElementById("p_edad");
let p_sexo = document.getElementById("p_sexo");
let p_fechaNac = document.getElementById("p_fechaNac");
let p_email = document.getElementById("p_email");
let p_curp = document.getElementById("p_curp");
let p_domicilio = document.getElementById("p_domicilio");



function update(id_solicitante){



//    var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
   axios.post('../../update_solicitante', {
    id_solicitante: id_solicitante,
    nombre: document.getElementById('nombre').value,
    apellidoP: document.getElementById('apellidoP').value,
    apellidoM: document.getElementById('apellidoM').value,
    edad: document.getElementById('edad').value,
    sexo: document.getElementById('sexo').value,
    fechaNac: document.getElementById('fechaNac').value,
    inputEmail: document.getElementById('inputEmail').value,
    inputCurp: document.getElementById('inputCurp').value,
    inputDomicilio: document.getElementById('inputDomicilio').value,
    arrayImpuestos: JSON.stringify(array)
    
  })
  .then(function (response) {
    console.log(response.data);
    if (response.data.success == false) {

      Swal.fire({
      title: 'Error!',
      text: response.data.message,
      icon: 'error',
      confirmButtonText: 'Cool'
      })

      const errores = response.data.errors

      let claves = Object.keys(errores); // claves = ["nombre", "color", "macho", "edad"]
      for(let i=0; i< claves.length; i++){
        let clave = claves[i];
        if(clave != 'undefined'){
          p_nombre.innerHTML  = response.data.errors.nombre
          p_apellidoP.innerHTML  = response.data.errors.apellidoP
          p_apellidoM.innerHTML  = response.data.errors.apellidoM
          p_edad.innerHTML  = response.data.errors.edad
          p_sexo.innerHTML  = response.data.errors.sexo
          p_fechaNac.innerHTML  = response.data.errors.fechaNac
          p_email.innerHTML  = response.data.errors.inputEmail
          p_curp.innerHTML  = response.data.errors.inputCurp
          p_domicilio.innerHTML  = response.data.errors.inputDomicilio

        }
        if(response.data.errors.nombre == undefined){
            document.getElementsByClassName("text-danger"); 
            p_nombre.classList.remove("text-danger");
            p_nombre.innerHTML = "";
          }  
          if(response.data.errors.apellidoP == undefined ){
            document.getElementsByClassName("text-danger"); 
            p_apellidoP.classList.remove("text-danger");
            p_apellidoP.innerHTML  = "";
          }
          if (response.data.errors.apellidoM == undefined) {
            document.getElementsByClassName("text-danger");
            p_apellidoM.classList.remove("text-danger");
            p_apellidoM.innerHTML  = "";
          }
          if (response.data.errors.edad == undefined ) {
            document.getElementsByClassName("text-danger");
            p_edad.classList.remove("text-danger");
            p_edad.innerHTML  = "";
          }
          if (response.data.errors.sexo == undefined) {
            document.getElementsByClassName("text-danger");
            p_sexo.classList.remove("text-danger"); 
            p_sexo.innerHTML  = "";
          }
          if (response.data.errors.fechaNac == undefined) {
            document.getElementsByClassName("text-danger");
            p_fechaNac.classList.remove("text-danger");
            p_fechaNac.innerHTML  = "";
          }
          if (response.data.errors.inputEmail == undefined) {
            document.getElementsByClassName("text-danger");
            p_email.classList.remove("text-danger");
            p_email.innerHTML  = "";
          }
          if (response.data.errors.inputCurp == undefined ) {
            document.getElementsByClassName("text-danger");
            p_curp.classList.remove("text-danger");
            p_curp.innerHTML  = "";
          }
          if (response.data.errors.inputDomicilio == undefined) {
            document.getElementsByClassName("text-danger");
            p_domicilio.classList.remove("text-danger");
            p_domicilio.innerHTML  = "";
          }
                
        }

    }    
    
    if (response.data.status == 1) {

      Swal.fire({
      title: 'Exito!',
      text: 'Datos guardados correctamente',
      icon: 'success',
      confirmButtonText: 'Cool'
      })

      // this.limpiar_campos()
      // tbody.innerHTML = '';
      // this.cerrar_modal()
      // var myModal = new bootstrap.Modal(document.getElementById('exampleModal'));
          myModal.hide();

    }  
    // if (response.data.status == 3) {

    //   Swal.fire({
    //   title: 'Error!',
    //   text: 'Debe agregar por lo menos un ingreso para continuar',
    //   icon: 'error',
    //   confirmButtonText: 'Cool'
    //   })

    // } 

   
  
   
  
  })
  .catch(function (error) {
    // console.log(error);
    // Swal.fire({
    // title: 'Error!',
    // text: 'Do you want to continue',
    // icon: 'error',
    // confirmButtonText: 'Cool'
    // })
  });
   
};

function addIngreso(){


    axios.post('../../agregar_ingreso', {
    id_solicitante: id_solicitante,
    nombreEmpresa:document.getElementById('nombreEmpresa').value,
    tipoComprobante:document.getElementById('tipoComprobante').value,
    salarioNeto:document.getElementById('salarioNeto').value,
    salarioBruto:document.getElementById('salarioBruto').value,
    tipoEmpleo:document.getElementById('tipoEmpleo').value,
    fechaIngreso:document.getElementById('fechaIngreso').value
    
  })
  .then(function (response) {
    console.log(response.data);
    // if (response.data.success == false) {

    //   Swal.fire({
    //   title: 'Error!',
    //   text: response.data.message,
    //   icon: 'error',
    //   confirmButtonText: 'Cool'
    //   })

    //   const errores = response.data.errors

    //   let claves = Object.keys(errores); // claves = ["nombre", "color", "macho", "edad"]
    //   for(let i=0; i< claves.length; i++){
    //     let clave = claves[i];
    //     if(clave != 'undefined'){
    //       p_nombre.innerHTML  = response.data.errors.nombre
    //       p_apellidoP.innerHTML  = response.data.errors.apellidoP
    //       p_apellidoM.innerHTML  = response.data.errors.apellidoM
    //       p_edad.innerHTML  = response.data.errors.edad
    //       p_sexo.innerHTML  = response.data.errors.sexo
    //       p_fechaNac.innerHTML  = response.data.errors.fechaNac
    //       p_email.innerHTML  = response.data.errors.inputEmail
    //       p_curp.innerHTML  = response.data.errors.inputCurp
    //       p_domicilio.innerHTML  = response.data.errors.inputDomicilio

    //     }
    //     if(response.data.errors.nombre == undefined){
    //         document.getElementsByClassName("text-danger"); 
    //         p_nombre.classList.remove("text-danger");
    //         p_nombre.innerHTML = "";
    //       }  
    //       if(response.data.errors.apellidoP == undefined ){
    //         document.getElementsByClassName("text-danger"); 
    //         p_apellidoP.classList.remove("text-danger");
    //         p_apellidoP.innerHTML  = "";
    //       }
    //       if (response.data.errors.apellidoM == undefined) {
    //         document.getElementsByClassName("text-danger");
    //         p_apellidoM.classList.remove("text-danger");
    //         p_apellidoM.innerHTML  = "";
    //       }
    //       if (response.data.errors.edad == undefined ) {
    //         document.getElementsByClassName("text-danger");
    //         p_edad.classList.remove("text-danger");
    //         p_edad.innerHTML  = "";
    //       }
    //       if (response.data.errors.sexo == undefined) {
    //         document.getElementsByClassName("text-danger");
    //         p_sexo.classList.remove("text-danger"); 
    //         p_sexo.innerHTML  = "";
    //       }
    //       if (response.data.errors.fechaNac == undefined) {
    //         document.getElementsByClassName("text-danger");
    //         p_fechaNac.classList.remove("text-danger");
    //         p_fechaNac.innerHTML  = "";
    //       }
    //       if (response.data.errors.inputEmail == undefined) {
    //         document.getElementsByClassName("text-danger");
    //         p_email.classList.remove("text-danger");
    //         p_email.innerHTML  = "";
    //       }
    //       if (response.data.errors.inputCurp == undefined ) {
    //         document.getElementsByClassName("text-danger");
    //         p_curp.classList.remove("text-danger");
    //         p_curp.innerHTML  = "";
    //       }
    //       if (response.data.errors.inputDomicilio == undefined) {
    //         document.getElementsByClassName("text-danger");
    //         p_domicilio.classList.remove("text-danger");
    //         p_domicilio.innerHTML  = "";
    //       }
                
    //     }

    // }    
    
    if (response.data.status == 1) {

      Swal.fire({
      title: 'Exito!',
      text: 'Datos guardados correctamente',
      icon: 'success',
      confirmButtonText: 'Cool'
      })

      // this.limpiar_campos()
      // tbody.innerHTML = '';
      // this.cerrar_modal()
      // var myModal = new bootstrap.Modal(document.getElementById('exampleModal'));
          myModal.hide();

    }  


   
  
   
  
  })
  .catch(function (error) {
    // console.log(error);
    // Swal.fire({
    // title: 'Error!',
    // text: 'Do you want to continue',
    // icon: 'error',
    // confirmButtonText: 'Cool'
    // })
  });
 
};

function limpiar_campos(){
    document.getElementById('nombre').value = '',
    document.getElementById('apellidoP').value = '',
    document.getElementById('apellidoM').value = '',
    document.getElementById('edad').value = '',
    document.getElementById('sexo').value = '',
    document.getElementById('fechaNac').value = '',
    document.getElementById('inputEmail').value = '',
    document.getElementById('inputCurp').value = '',
    document.getElementById('inputDomicilio').value = ''

  
};

// function cerrar_modal(){
  
// };
</script>
</body>


@endsection

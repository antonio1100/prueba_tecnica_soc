
@section('modalIngresos')
<body>
<div class="modal fade" id="ModalParaIngresos" aria-hidden="true" aria-labelledby="ModalParaIngresosleLabel2" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalToggleLabel2">Agregar ingreso</h5>
        </div>
        <div class="modal-body">
          <!-- Hide this modal and show the first with the button below. -->

            <form class="row g-3 needs-validation" novalidate>
              <div class="col-4">              
                  <input type="text" class="form-control" id="nombreEmpresa" placeholder="Nombre de la empresa" aria-label="First name">
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="tipoComprobante" placeholder="Tipo comprobante" aria-label="Last name">
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="salarioNeto" placeholder="Salario neto" aria-label="Last name">
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="salarioBruto" placeholder="Salario bruto" aria-label="Last name">
              </div>
              <div class="col-4">
                  <input type="text" class="form-control" id="tipoEmpleo" placeholder="Tipo de empleo" aria-label="Last name">
              </div>
              <div class="col-4">
                  <input type="date" class="form-control" id="fechaIngreso" placeholder="Fecha ingreso">
              </div>
              <!-- <button type="button" class="btn btn-primary"  onclick='proccess();'>Guardar datos</button> -->
              <!-- <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button> -->
          
            </form>

           
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" data-bs-target="#exampleModalToggle" data-bs-toggle="modal" data-bs-dismiss="modal">Cancelar</button>
           <button type="button" class="btn btn-primary" id="btn_ingreso" onclick='ingreso();'>Guardar ingreso</button>
        </div>
      </div>
    </div>
  </div>
</body>
<script>

  
//Ocultar modal


function proccess(){
//    var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
   axios.post('agregar_solicitante', {
    nombre: document.getElementById('nombre').value,
    apellidoP: document.getElementById('apellidoP').value,
    apellidoM: document.getElementById('apellidoM').value,
    edad: document.getElementById('edad').value,
    sexo: document.getElementById('sexo').value,
    fechaNac: document.getElementById('fechaNac').value,
    inputEmail: document.getElementById('inputEmail').value,
    inputCurp: document.getElementById('inputCurp').value,
    inputDomicilio: document.getElementById('inputDomicilio').value
  })
  .then(function (response) {

    if (response.data.status == 1) {

      Swal.fire({
      title: 'Exito!',
      text: 'Datos guardados correctamente',
      icon: 'success',
      confirmButtonText: 'Cool'
      })

      this.limpiar_campos()
      // this.cerrar_modal()
      // var myModal = new bootstrap.Modal(document.getElementById('exampleModal'));
      //     myModal.hide();

    }  
  
   
  
  })
  .catch(function (error) {
    // console.log(error);
    // Swal.fire({
    // title: 'Error!',
    // text: 'Do you want to continue',
    // icon: 'error',
    // confirmButtonText: 'Cool'
    // })
  });
   
};

function limpiar_campos(){
    document.getElementById('nombre').value = '',
    document.getElementById('apellidoP').value = '',
    document.getElementById('apellidoM').value = '',
    document.getElementById('edad').value = '',
    document.getElementById('sexo').value = '',
    document.getElementById('fechaNac').value = '',
    document.getElementById('inputEmail').value = '',
    document.getElementById('inputCurp').value = '',
    document.getElementById('inputDomicilio').value = ''
};

// function cerrar_modal(){
  
// };
</script>
</body>


@endsection

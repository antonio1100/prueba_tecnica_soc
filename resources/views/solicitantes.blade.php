@extends('layout')
@section('content')
<body>

<button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModalToggle">Agregar solicitante</button>
    <br><br>
    <table class="table table-bordered">
        <thead>
            <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido paterno</th>
            <th scope="col">Apellido materno</th>
            <th scope="col">Edad</th>
            <th scope="col">Sexo</th>
            <th scope="col">Correo electónico</th>
            <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $item)
            <tr>
            <td>{{$item->nombre}}</td>
            <td>{{$item->apellido_paterno}}</td>
            <td>{{$item->apellido_materno}}</td>
            <td>{{$item->edad}}</td>
            <td>{{$item->sexo}}</td>
            <td>{{$item->email}}</td>
            <td>
            <a href="editar/solicitante/{{$item->id}}" class="btn btn-warning " ><i  class="bi-pen"></i></a>
                <!-- <a  class="btn btn-warning " onclick="editar_solicitud({{$item->id}})"><i  class="bi-pen"></i></a> -->
                <!-- <a href="" class="btn btn-warning " ><i  class="bi-trash"></i></a> -->
            </td>
            <!-- <td><button type="button" class="btn btn-outline-primary" ></button></td> -->
            </tr>
            @endforeach
        </tbody>
    </table>


    @section('modal')
        @include('modalSolicitante')
    @show
             

@endsection  



<script>
   function editar_solicitud(id){
    console.log("editar",id)
            // axios.post('editar_solicitante/'+id, {
            axios.get('editar_solicitante/'+id, {
            solicitante_id: id          
        })
        .then(function (response) {
            console.log(response.data);
        
            
            if (response.data.status == 1) {

            Swal.fire({
            title: 'Exito!',
            text: 'Datos guardados correctamente',
            icon: 'success',
            confirmButtonText: 'Cool'
            })

            }  
            // if (response.data.status == 3) {

            // Swal.fire({
            // title: 'Error!',
            // text: 'Debe agregar por lo menos un ingreso para continuar',
            // icon: 'error',
            // confirmButtonText: 'Cool'
            // })

            // } 

        
        
        
        
        })
        .catch(function (error) {
            // console.log(error);
            // Swal.fire({
            // title: 'Error!',
            // text: 'Do you want to continue',
            // icon: 'error',
            // confirmButtonText: 'Cool'
            // })
        });
    }

</script>
</body>
   
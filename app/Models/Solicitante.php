<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitante extends Model
{
    protected $table = 'Solicitante';
    use HasFactory;

    public function ingresos()
    {
        return $this->hasMany('App\Models\Ingreso','solicitante_id','id');
    }
}

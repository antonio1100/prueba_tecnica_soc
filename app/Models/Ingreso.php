<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $table = 'ingresos';
    use HasFactory;

    public function solicitante()
    {
    return $this->belongsTo(Solicitante::class,'solicitante_id');
    }
}

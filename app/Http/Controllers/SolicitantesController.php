<?php

namespace App\Http\Controllers;
use App\Models\Solicitante;
use App\Models\Ingreso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SolicitantesController extends Controller
{
    public function index(){
        $solicitantes_db = Solicitante::all();
        return view('solicitantes')->with('data',$solicitantes_db);
    }

    public function agregar_solicitante(Request $request) {

        $validator = Validator::make($request->all(), [
            'nombre'=>  'required|min:2|max:60',
            'apellidoP'=>  'required|min:2|max:60',
            'apellidoM'=> 'required|min:2|max:60',
            'edad'=> 'required',
            'sexo'=> 'required',
            'fechaNac'=> 'required',
            'fechaNac'=> 'required',
            'inputEmail'=>  'required|email',
            'inputCurp'=> 'required',
            // 'telefono'=> 'required|numeric|digits:10',
            'inputDomicilio'=>  'required|min:20|max:200',

            
        ]);

     
        if ($validator->fails()) {
            $success = false;
            $message = 'Ocurrió un problema al guardar la informacion, verifique que los campo del formulario cumplan con los requisitos solicitados. ';
            $errors  = $validator->errors();

            $array = json_decode(  $errors , true );

    
            return [

                'message'   => $message,
                'success'   => $success,
                'errors'    => $errors,
            ];
    
        }
        // dd($request->all());
        $array_ingresos = json_decode($request->arrayImpuestos,true);
        // dd($array_ingresos);
        if (empty($array_ingresos)) {
        
            return response()->json(['response' => 'success', 'status' => 3],200);
        }

        $guardar_solicitante = new Solicitante();
        $guardar_solicitante->nombre = $request->nombre;
        $guardar_solicitante->apellido_paterno = $request->apellidoP;
        $guardar_solicitante->apellido_materno = $request->apellidoM;
        $guardar_solicitante->edad = $request->edad;
        $guardar_solicitante->fecha_nacimiento = $request->fechaNac;
        $guardar_solicitante->sexo = $request->sexo;
        $guardar_solicitante->curp = $request->inputCurp;
        $guardar_solicitante->email = $request->inputEmail;
        $guardar_solicitante->domicilio = $request->inputDomicilio;
        $guardar_solicitante->save();

        foreach ( $array_ingresos as $key => $value) {
        //    dd($value);
           $guardar_ingreso = new Ingreso();
           $guardar_ingreso->solicitante_id = $guardar_solicitante->id;
           $guardar_ingreso->nombre_empresa = $value['nombreEmpresa'];
           $guardar_ingreso->tipo_comprobante = $value['tipoComprobante'];
           $guardar_ingreso->salario_bruto = ($value['salarioBruto'])? $value['salarioBruto']:null;
           $guardar_ingreso->salario_neto = $value['salarioNeto'];
           $guardar_ingreso->tipo_empleo = $value['tipoEmpleo'];
           $guardar_ingreso->fecha_ingreso = $value['fechaIngreso'];
           $guardar_ingreso->save();
        }

       
      
        return response()->json(['response' => 'success', 'status' => 1],200);
    }


    public function editar_solicitante(Request $request,$id) {


        $placeId = $request->id;
  
        $datos_solicitante = Solicitante::
        with(['ingresos' => function ($query) use ($placeId) {
            $query->where('solicitante_id', '=', $placeId);
        }])
        ->where('id','=',$placeId)
        ->get();
        
        return view('solicitanteEdit')->with('data_solicitante',$datos_solicitante);
    }

    public function update_solicitante(Request $request) {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'nombre'=>  'required|min:2|max:60',
            'apellidoP'=>  'required|min:2|max:60',
            'apellidoM'=> 'required|min:2|max:60',
            'edad'=> 'required',
            'sexo'=> 'required',
            'fechaNac'=> 'required',
            'fechaNac'=> 'required',
            'inputEmail'=>  'required|email',
            'inputCurp'=> 'required',
            // 'telefono'=> 'required|numeric|digits:10',
            'inputDomicilio'=>  'required|min:20|max:200',

            
        ]);

     
        if ($validator->fails()) {
            $success = false;
            $message = 'Ocurrió un problema al guardar la informacion, verifique que los campo del formulario cumplan con los requisitos solicitados. ';
            $errors  = $validator->errors();

            $array = json_decode(  $errors , true );

    
            return [

                'message'   => $message,
                'success'   => $success,
                'errors'    => $errors,
            ];
    
        }
        // dd($request->all());
        $array_ingresos = json_decode($request->arrayImpuestos,true);
        // dd($array_ingresos);
        // if (empty($array_ingresos)) {
        
        //     return response()->json(['response' => 'success', 'status' => 3],200);
        // }

    

        $update_solicitante = Solicitante::find($request->id_solicitante);
        $update_solicitante->nombre = $request->nombre;
        $update_solicitante->apellido_paterno = $request->apellidoP;
        $update_solicitante->apellido_materno = $request->apellidoM;
        $update_solicitante->edad = $request->edad;
        $update_solicitante->fecha_nacimiento = $request->fechaNac;
        $update_solicitante->sexo = $request->sexo;
        $update_solicitante->curp = $request->inputCurp;
        $update_solicitante->email = $request->inputEmail;
        $update_solicitante->domicilio = $request->inputDomicilio;
        $update_solicitante->save();

        // foreach ( $array_ingresos as $key => $value) {
        // //    dd($value);
        //    $guardar_ingreso = new Ingreso();
        //    $guardar_ingreso->solicitante_id = $guardar_solicitante->id;
        //    $guardar_ingreso->nombre_empresa = $value['nombreEmpresa'];
        //    $guardar_ingreso->tipo_comprobante = $value['tipoComprobante'];
        //    $guardar_ingreso->salario_bruto = ($value['salarioBruto'])? $value['salarioBruto']:null;
        //    $guardar_ingreso->salario_neto = $value['salarioNeto'];
        //    $guardar_ingreso->tipo_empleo = $value['tipoEmpleo'];
        //    $guardar_ingreso->fecha_ingreso = $value['fechaIngreso'];
        //    $guardar_ingreso->save();
        // }

       
      
        return response()->json(['response' => 'success', 'status' => 1],200);
    }
}
